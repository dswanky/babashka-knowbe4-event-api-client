(ns babashka-knowbe4-event-api-client)

(require '[clojure.string :as str])
(require '[org.httpkit.client :as http])
(require '[clojure.edn :as edn])
(require '[cheshire.core :as json])

(defn api-get-request
  ([params api-key url]
   (let [options {:query-params params
                  :headers {"Accept" "application/json"
                            "Authorization" (str "Bearer " api-key)}}
         {:keys [status headers body error] :as resp} @(http/get url options)]
     resp))
  ([api-key url]
   (api-get-request {} api-key url)))


(defn get-all-users
  "Get all knowbe4 events"
  [api-key]
  (let [users (api-get-request {:status "active"
                                :per_page 500}
                                api-key
                                "https://us.api.knowbe4.com/v1/users")]
    users))

(defn save-all-users
  "Get and save all knowbe4 events to a file"
  [api-key out-file]
  (spit out-file (str (get-all-users api-key))))

(defn concat-coll
  "Add an item to the end of a collection"
  [coll item]
  (cond
    (some-fn map? vector? set? coll) (conj coll item)
    :else (concat coll (list item))))

(defn email-username
  "Get the username from an email address"
  [email]
  (first (str/split email #"@")))

(defn resp-usernames
  "Get the usernames from a response"
  [resp]
  (let [response (edn/read-string resp)
        resp-data (json/parse-string (:body response))
        emails (map #(first (remove (fn [x] (nil? x)) (str/split (get % "email") #" "))) resp-data)
        usernames (map email-username emails)]
    usernames))

(defn -main
  []
  (let [api-key (slurp "api.key")
        usernames (resp-usernames (slurp "users.out"))]
    ;;(save-all-users api-key "users.out")
    (println usernames)
    ;;(map #(do (println %) (get % "email")) resp-data)
    ))
